package com.example.android_andrey_client;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.google.android.material.snackbar.Snackbar;
import com.google.ar.core.Frame;
import com.google.ar.core.Plane;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import uk.co.appoly.arcorelocation.LocationMarker;
import uk.co.appoly.arcorelocation.LocationScene;
import uk.co.appoly.arcorelocation.utils.ARLocationPermissionHelper;

public class MainActivity extends AppCompatActivity {

    private boolean installRequested;
    private boolean hasFinishedLoading = false;

    private Snackbar loadingMessageSnackbar = null;

    private ArSceneView arSceneView;
    private LocationScene locationScene;

    Intent intent2;
    private String TAG = "APP";
    // Renderables for this example
    private ModelRenderable andyRenderable;
    private ViewRenderable exampleLayoutRenderable;
    private ViewRenderable exampleLayoutRenderable2;
    //new
    private ArrayList<ViewRenderable> renderables = new ArrayList<>();
    private ArrayList<LocationMarker> locationMarkers = new ArrayList<>();
//    private ArrayList<Building> buildings = new ArrayList<>();
//    private ArrayList<CompletableFuture<ViewRenderable>> futures = new ArrayList<>();

    TextView tx1;
    TextView tx2;
    TextView tx3;
    ConstraintLayout arlayout;

    //http client method
//    public void GetZone(float x, float y, float z){
//        OkHttpClient client = new OkHttpClient();
//        Request request = new Request.Builder()
//                .url("http://mrblackdog.ddns.net:533/rawmeas/getMeasurments?Name=Client&X="+x+"&Y="+y+"&Z="+z)
//                .build();
//        //client.setConnectTimeout(15, TimeUnit.SECONDS);
//
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                e.printStackTrace();
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
//            }
//        });
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        arSceneView = findViewById(R.id.ar_scene_view);
        arlayout = findViewById(R.id.ar_main);
//        getSupportActionBar().hide();
        WebSocketGolos.getLocationCallBack = this::setTargetLocation;
        tx1 = findViewById(R.id.textView5);
        tx2 = findViewById(R.id.textView6);
        tx3 = findViewById(R.id.textView8);
        Button bt = findViewById(R.id.button2);
        bt.setEnabled(false);
        bt.setVisibility(View.GONE);
        // Build a renderable from a 2D View.
        CompletableFuture<ViewRenderable> exampleLayout =
                ViewRenderable.builder()
                        .setView(this, R.layout.example_layout)
                        .build();

        CompletableFuture<ViewRenderable> exampleLayout2 =
                ViewRenderable.builder()
                        .setView(this, R.layout.example_layout2)
                        .build();
//        Building building1 = new Building("XD","XD","XD",55.75483550215651, 37.706543287440375);
//        Building building2 = new Building("XD2","XD2","XD2",55.75421967298693, 37.70588882848575);
//        Building building3 = new Building("XD3","XD3","XD3",55.753893244052755, 37.70771318430918);
//        buildings.add(building1);
//        buildings.add(building2);
//        buildings.add(building3);
//        for (int i =0;i<buildings.size();i++)
//        {
//            CompletableFuture<ViewRenderable> Layout =
//                    ViewRenderable.builder()
//                            .setView(this, R.layout.example_layout)
//                            .build();
//            CompletableFuture.allOf(
//                   Layout)
//                    .handle(
//                            (notUsed, throwable) -> {
//                                // When you build a Renderable, Sceneform loads its resources in the background while
//                                // returning a CompletableFuture. Call handle(), thenAccept(), or check isDone()
//                                // before calling get().
//
//                                if (throwable != null) {
//                                    DemoUtils.displayError(this, "Unable to load renderables", throwable);
//                                    return null;
//                                }
//
//                                try {
//                                    //for new project
//                                    renderables.add(Layout.get());
//                                    //andyRenderable = andy.get();
//                                    hasFinishedLoading = true;
//
//                                } catch (InterruptedException | ExecutionException ex) {
//                                    DemoUtils.displayError(this, "Unable to load renderables", ex);
//                                }
//
//                                return null;
//                            });
//        }
//        renderables.ensureCapacity(buildings.size());
        CompletableFuture.allOf(
                exampleLayout,exampleLayout2)
                .handle(
                        (notUsed, throwable) -> {
                            // When you build a Renderable, Sceneform loads its resources in the background while
                            // returning a CompletableFuture. Call handle(), thenAccept(), or check isDone()
                            // before calling get().

                            if (throwable != null) {
                                DemoUtils.displayError(this, "Unable to load renderables", throwable);
                                return null;
                            }

                            try {
                                //for new project
                                exampleLayoutRenderable = exampleLayout.get();
                                exampleLayoutRenderable2 = exampleLayout2.get();
                                //andyRenderable = andy.get();
                                hasFinishedLoading = true;

                            } catch (InterruptedException | ExecutionException ex) {
                                DemoUtils.displayError(this, "Unable to load renderables", ex);
                            }

                            return null;
                        });

        //3d model loader
        CompletableFuture<ModelRenderable> andy = ModelRenderable.builder()
                .setSource(this, Uri.parse("person_icon_v8.sfb"))
                .build();
        CompletableFuture.allOf(andy)
                .handle(
                        (notUsed, throwable) ->
                        {
                            if (throwable != null) {
                                DemoUtils.displayError(this, "Unable to load renderables", throwable);
                                return null;
                            }

                            try {
                                andyRenderable = andy.get();

                            } catch (InterruptedException | ExecutionException ex) {
                                DemoUtils.displayError(this, "Unable to load renderables", ex);
                            }
                            return null;
                        });

        // Set an update listener on the Scene that will hide the loading message once a Plane is
        // detected.
        arSceneView.getScene().addOnUpdateListener(
                frameTime -> {
                    if (!hasFinishedLoading) {
                        return;
                    }

                    if (locationScene == null)
                    {
                        // If our locationScene object hasn't been setup yet, this is a good time to do it
                        // We know that here, the AR components have been initiated.
                        locationScene = new LocationScene(this, arSceneView);

                        // Now lets create our location markers.
                        // First, a layout
//                        LocationMarker andyLocationMarker = new LocationMarker(
//                                44.164716, 38.998964,
//                                getAndy()
//                        );

                        LocationMarker layoutLocationMarker = new LocationMarker(
                                55.754804456416906, 37.70791107609131,
                                getExampleView()
                        );

                        LocationMarker layoutLocationMarker2 = new LocationMarker(
                                55.754709, 37.706672,
                                getExampleView2()
                        );

//                        new
//                        for (int i=0;i<buildings.size();i++)
//                         {
//                             LocationMarker lm = new LocationMarker(buildings.get(i).getLatitude(),buildings.get(i).getLongitude(),getRenderableView(i));
//                             View eView = renderables.get(i).getView();
//                             lm.setRenderEvent(anchornode ->{
//                                TextView distanceTextView = eView.findViewById(R.id.textView2);
//                                TextView coordTextView = eView.findViewById(R.id.textView9);
//                                distanceTextView.setText(anchornode.getDistance() + "M");
//                                coordTextView.setText(lm.latitude + " " + lm.longitude);
////                              lm.setScaleModifier(1f);
////                              lm.setHeight(1.2f);
//                                //включение и отключение метки,если расстояние меньше заданного. работает.
//                                lm.node.setEnabled(lm.anchorNode.getDistance() <= 350);
//                            });
//                            //locationMarkers.add(lm);
//                            locationScene.mLocationMarkers.add(lm);
//                        }
                     /*   for (LocationMarker lm :locationScene.mLocationMarkers)
                        {
                            lm.setRenderEvent(anchornode -> {

                                View eView = exampleLayoutRenderable.getView();

                                TextView distanceTextView = eView.findViewById(R.id.textView2);
                                TextView coordTextView = eView.findViewById(R.id.textView9);

                                distanceTextView.setText(anchornode.getDistance() + "M");
                                coordTextView.setText(lm.latitude + " " + lm.longitude);
                                //включение и отключение метки,если расстояние меньше заданного. работает.
                                if(lm.anchorNode.getDistance() > 350)
                                {
                                    lm.node.setEnabled(false);
                                    tx2.setText("out of range");
                                }
                                else
                                {
                                    tx2.setText(anchornode.getDistance() + "M");
                                    // tx3.setText("out of vision");
                                    lm.node.setEnabled(true);
                                }

                            });
                        }*/

                      /*  andyLocationMarker.setRenderEvent(anchornode -> {
                            locationScene.mLocationMarkers.get(0).setScaleModifier(1f);
                            locationScene.mLocationMarkers.get(0).setHeight(1.2f);
                            //    locationScene.mLocationMarkers.get(1).setScaleModifier(0.4f);
                           locationScene.mLocationMarkers.get(0).setScalingMode(LocationMarker.ScalingMode.GRADUAL_FIXED_SIZE);
                        });*/

                        layoutLocationMarker.setRenderEvent(anchornode -> {

                            View eView = exampleLayoutRenderable.getView();

                            TextView distanceTextView = eView.findViewById(R.id.textView2);
                            TextView coordTextView = eView.findViewById(R.id.textView9);

                            distanceTextView.setText(anchornode.getDistance() + "M");
                            coordTextView.setText(layoutLocationMarker.latitude + " " + layoutLocationMarker.longitude);
                           // locationScene.mLocationMarkers.get(0).setScaleModifier(0.2f);
                        //    locationScene.mLocationMarkers.get(1).setScaleModifier(0.4f);
                            locationScene.mLocationMarkers.get(0).setScalingMode(LocationMarker.ScalingMode.GRADUAL_TO_MAX_RENDER_DISTANCE);
                            //включение и отключение метки,если расстояние меньше заданного. работает.
                            if(locationScene.mLocationMarkers.get(0).anchorNode.getDistance() > 350)
                            {
                                locationScene.mLocationMarkers.get(0).node.setEnabled(false);
                          //      tx2.setText("out of range");
                            }
                            else
                            {
                          //      tx2.setText(anchornode.getDistance() + "M");
                                // tx3.setText("out of vision");
                                locationScene.mLocationMarkers.get(0).node.setEnabled(true);
                            }
                        });
                        // Adding the marker
                        // An example "onRender" event, called every frame
                        // Updates the layout with the markers distance
                        layoutLocationMarker2.setRenderEvent(anchornode -> {
                            View eView2 = exampleLayoutRenderable2.getView();
                            TextView distanceTextView = eView2.findViewById(R.id.textView2);
                            TextView coordTextView = eView2.findViewById(R.id.textView9);
                            locationScene.mLocationMarkers.get(1).setScaleModifier(0.2f);
                            locationScene.mLocationMarkers.get(1).anchorNode.setHeight(1.5f);
                            //locationScene.mLocationMarkers.get(1).setScalingMode(LocationMarker.ScalingMode.GRADUAL_TO_MAX_RENDER_DISTANCE);
                            distanceTextView.setText(anchornode.getDistance() + "M");
                            coordTextView.setText(layoutLocationMarker2.latitude + " " + layoutLocationMarker2.longitude);
                            //включение и отключение метки,если расстояние меньше заданного. работает.
                            if(locationScene.mLocationMarkers.get(1).anchorNode.getDistance() > 350)
                            {
                                locationScene.mLocationMarkers.get(1).node.setEnabled(false);
                          //      tx3.setText("out of range");
                            }
                            else
                            {
                          //      tx3.setText(anchornode.getDistance() + "M");
                                // tx3.setText("out of vision");
                                locationScene.mLocationMarkers.get(1).node.setEnabled(true);
                            }
                        });


                        locationScene.mLocationMarkers.add(layoutLocationMarker);
                        // Adding the marker
                        // An example "onRender" event, called every frame
                       // Updates the layout with the markers distance

                     //   tx2.setText(layoutLocationMarker.latitude + " " + layoutLocationMarker.longitude);

                         //   Adding a simple location marker of a 3D model
//                        locationScene.mLocationMarkers.add(
//                                new LocationMarker(
//                                        55.74390627167618, 37.86765408942005,
//                                        getAndy()));
                    }
                    //Frame process
                    Frame frame = arSceneView.getArFrame();
                    if (frame == null) {
                        return;
                    }

                    if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
                        return;
                    }

                    if (locationScene != null) {
                        try {
                            tx1.setText(locationScene.deviceLocation.currentBestLocation.getLatitude() + " " +
                                    locationScene.deviceLocation.currentBestLocation.getLongitude());
                        }
                        catch (NullPointerException ex)
                        {
                            tx1.setText("No Current location");
                            //tx2.setText("No Current location");
                        }
                        locationScene.processFrame(frame);
                    }

                    if (loadingMessageSnackbar != null) {
                        for (Plane plane : frame.getUpdatedTrackables(Plane.class)) {
                            if (plane.getTrackingState() == TrackingState.TRACKING) {
                                //hideLoadingMessage();
                            }
                        }
                    }

                });
        /*  arSceneView
                .getScene()
                .setOnUpdateListener(
               // .addOnUpdateListener(this::OnUpdateFrame);
                        frameTime -> {
                            if (!hasFinishedLoading) {
                                return;
                            }

                            if (locationScene == null) {
                                // If our locationScene object hasn't been setup yet, this is a good time to do it
                                // We know that here, the AR components have been initiated.
                                locationScene = new LocationScene(this, arSceneView);

                                // Now lets create our location markers.
                                // First, a layout
                                LocationMarker layoutLocationMarker = new LocationMarker(
                                        37.703365, 55.756100,
                                        getExampleView()
                                );

                                // An example "onRender" event, called every frame
                                // Updates the layout with the markers distance
                                layoutLocationMarker.setRenderEvent(node -> {
                                    View eView = exampleLayoutRenderable.getView();
                                    TextView distanceTextView = eView.findViewById(R.id.textView2);
                                    distanceTextView.setText(node.getDistance() + "M");
                                });
                                // Adding the marker
                                locationScene.mLocationMarkers.add(layoutLocationMarker);

                                tx2.setText(layoutLocationMarker.latitude + " " + layoutLocationMarker.longitude);


                                // Adding a simple location marker of a 3D model

                    }

                            //Frame process
                            Frame frame = arSceneView.getArFrame();
                            if (frame == null) {
                                return;
                            }

                            if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
                                return;
                            }
                            //locationScene.mLocationMarkers.get(0)

                            if (locationScene != null) {

                                try {

                                   if(!locationScene.mLocationMarkers.isEmpty())
                                    {

                                        //tx3.setText("xd");
                                        if(locationScene.mLocationMarkers.get(1).anchorNode.getDistance() > 100)
                                        {
                                           locationScene.mLocationMarkers.get(0).node.setEnabled(false);
                                           // tx3.setText(locationScene.mLocationMarkers.);
                                        }
                                        else
                                        {
                                            tx3.setText("out of vision");
                                        }
                                    }

                            //        tx1.setText(locationScene.deviceLocation.currentBestLocation.getLatitude() + " " +
                              //              locationScene.deviceLocation.currentBestLocation.getLongitude());

                                }
                                catch (NullPointerException ex)
                                {
                           //         tx1.setText("No Current location");
                                    // tx2.setText("No Current location");
                                }
                                locationScene.processFrame(frame);
                            }

                            if (loadingMessageSnackbar != null) {
                                for (Plane plane : frame.getUpdatedTrackables(Plane.class)) {
                                    if (plane.getTrackingState() == TrackingState.TRACKING) {
                                      //  hideLoadingMessage();
                                    }
                                }
                            }
                        }); */
    }

    public void setTargetLocation(double latitude, double longitude) {
        //tx2.setText(latitude + " " + longitude);
        Log.d(TAG,"Location:" + String.valueOf(latitude)+ " " + String.valueOf(longitude));
        locationScene.mLocationMarkers.get(0).latitude = latitude;
        locationScene.mLocationMarkers.get(0).longitude = longitude;
        locationScene.mLocationMarkers.get(1).latitude = latitude;
        locationScene.mLocationMarkers.get(1).longitude = longitude;

    }

  /*  private void OnUpdateFrame(FrameTime frameTime)
    {
        if (!hasFinishedLoading) {
            return;
        }

        if (locationScene == null) {
            // If our locationScene object hasn't been setup yet, this is a good time to do it
            // We know that here, the AR components have been initiated.
            locationScene = new LocationScene(this, arSceneView);

            // Now lets create our location markers.
            // First, a layout
            LocationMarker layoutLocationMarker = new LocationMarker(
                    37.703365, 55.756100,
                    getExampleView()
            );

            // An example "onRender" event, called every frame
            // Updates the layout with the markers distance
            layoutLocationMarker.setRenderEvent(new LocationNodeRender() {
                @Override
                public void render(LocationNode node) {
                    View eView = exampleLayoutRenderable.getView();
                    TextView distanceTextView = eView.findViewById(R.id.textView2);
                    distanceTextView.setText(node.getDistance() + "M");
                }
            });
            // Adding the marker
            locationScene.mLocationMarkers.add(layoutLocationMarker);

            // Adding a simple location marker of a 3D model
                             /*   locationScene.mLocationMarkers.add(
                                        new LocationMarker(
                                                55.756100, 37.703365,
                                                getAndy()));

        }
        Frame frame = arSceneView.getArFrame();
        if (frame == null) {
            return;
        }

        if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
            return;
        }

        if (locationScene != null) {
            locationScene.processFrame(frame);
        }

        if (loadingMessageSnackbar != null) {
            for (Plane plane : frame.getUpdatedTrackables(Plane.class)) {
                if (plane.getTrackingState() == TrackingState.TRACKING) {
                    //  hideLoadingMessage();
                }
            }
        }
    } */

    /**
     * Make sure we call locationScene.resume();
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (locationScene != null) {
            locationScene.resume();
        }

        if (arSceneView.getSession() == null) {
            // If the session wasn't created yet, don't resume rendering.
            // This can happen if ARCore needs to be updated or permissions are not granted yet.
            try {
                Session session = DemoUtils.createArSession(this, installRequested);
                if (session == null) {
                    installRequested = ARLocationPermissionHelper.hasPermission(this);
                    return;
                } else {
                    arSceneView.setupSession(session);
                }
            } catch (UnavailableException e) {
                DemoUtils.handleSessionException(this, e);
            }
        }

        try {
            arSceneView.resume();
        } catch (CameraNotAvailableException ex) {
            DemoUtils.displayError(this, "Unable to get camera", ex);
            finish();
            return;
        }

        if (arSceneView.getSession() != null) {
           // showLoadingMessage();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        arSceneView.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
         intent2 = new Intent(MainActivity.this, StartActivity.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent intent = new Intent(MainActivity.this, StartActivity.class);
        startActivity(intent2);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private Node getExampleView() {
        Node base = new Node();
        base.setRenderable(exampleLayoutRenderable);
        Context c = this;
        //Add  listeners etc here
        View eView = exampleLayoutRenderable.getView();
        eView.setOnTouchListener((v, event) -> {
            Toast.makeText(
                    c, "Location marker touched.", Toast.LENGTH_LONG)
                    .show();
            //переход на другую активити
            //Intent intent = new Intent(MainActivity.this, QuestActivity.class);
            //startActivity(intent);
            //добавить отправку сообщения.
            return false;
        });
        return base;
    }

//    private Node getRenderableView(int i) {
//        Node base = new Node();
//        base.setRenderable(renderables.get(i));
//        Context c = this;
//        // Add  listeners etc here
//        View eView = renderables.get(i).getView();
//        eView.setOnTouchListener((v, event) -> {
//            Toast.makeText(
//                    c, "Location marker touched.", Toast.LENGTH_LONG)
//                    .show();
//            //переход на другую активити
//            //Intent intent = new Intent(MainActivity.this, QuestActivity.class);
//            //startActivity(intent);
//            //добавить отправку сообщения.
//            return false;
//        });
//        return base;
//    }

    private Node getExampleView2() {
        Node base = new Node();
        base.setRenderable(exampleLayoutRenderable2);
        Context c = this;
        // Add  listeners etc here
        View eView = exampleLayoutRenderable2.getView();
        eView.setOnTouchListener((v, event) -> {
            Toast.makeText(
                    c, "Location marker touched.", Toast.LENGTH_LONG)
                    .show();

            Intent intent = new Intent(MainActivity.this, TargetLocationActivity.class);
            startActivity(intent);
            //добавить отправку сообщения.
            return false;
        });
        return base;
    }

    private Node getAndy() {
        Node base = new Node();
        base.setRenderable(andyRenderable);
        Context c = this;
        base.setOnTapListener((v, event) -> {
            Toast.makeText(
                    c, "Andy touched.", Toast.LENGTH_LONG)
                    .show();
//            LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
//            ConstraintLayout layout = (ConstraintLayout) inflater.inflate(info_layout,arlayout ,false);
//            ConstraintLayout.LayoutParams LayoutParams =
//                    new ConstraintLayout.LayoutParams(
//                            ConstraintLayout.LayoutParams.WRAP_CONTENT,
//                            ConstraintLayout.LayoutParams.WRAP_CONTENT);
//            LayoutParams.bottomToBottom();
//            layout.setLayoutParams(LayoutParams);
//            ConstraintSet constraintSet = new ConstraintSet();
//            arlayout.addView(layout);
//            constraintSet.clone(arlayout);
//            constraintSet.connect(R.id.info, ConstraintSet.TOP,R.id.ar_main, ConstraintSet.TOP);
//            constraintSet.connect(R.id.info, ConstraintSet.BOTTOM,R.id.ar_main, ConstraintSet.BOTTOM);
//            constraintSet.connect(R.id.info, ConstraintSet.LEFT,R.id.ar_main, ConstraintSet.LEFT);
//            constraintSet.connect(R.id.info, ConstraintSet.RIGHT,R.id.ar_main, ConstraintSet.RIGHT);
//            //layout.setConstraintSet(constraintSet);
//            constraintSet.applyTo(arlayout);
        });
        return base;
    }
}
