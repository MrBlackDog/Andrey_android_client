package com.example.android_andrey_client;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TargetLocationActivity extends AppCompatActivity {

    private static final long LOCATION_RATE_GPS_MS = TimeUnit.MILLISECONDS.toMillis(1000L);
    //private static final long LOCATION_RATE_GPS_MS = TimeUnit.SECONDS.toMillis(1L);
    TextView textViewCoordinates;
    TextView textViewDistance;
    LocationManager mLocationManager;
    private String TAG = "APP";
    // private Context context;
    private LocationListener mLocationListener =
            new LocationListener() {

                @Override
                public void onProviderEnabled(String provider) {
                    try {
                        mLocationManager.requestLocationUpdates(provider, 0, 0, this);
                    } catch (SecurityException e) {

                    }
                }

                @Override
                public void onProviderDisabled(String provider) {

                }

                @Override
                public void onLocationChanged(Location location) {
                    if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                        Log.d(TAG, "onLocationChanged");
                        textViewCoordinates.setText(formatLocation(location));
                        StartActivity._ws.send("Location:" + location.getLatitude() + " " + location.getLongitude());
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }
            };


    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(mLocationListener);
    }

    private String formatLocation(Location location) {
        if (location == null)
            return "";
        return String.format(
                "Coordinates: lat = %1$.14f,\n lon = %2$.14f,\n time = %3$tF %3$tT",
                location.getLatitude(), location.getLongitude(), new Date(
                        location.getTime()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target_location);
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Bundle arguments = getIntent().getExtras();
//        context = context.getApplicationContext();
        if (arguments != null) {

        }
        textViewCoordinates = findViewById(R.id.textViewCoordinate);
        textViewDistance = findViewById(R.id.textViewDistance);
    }

    public void registerLocation() {
        boolean isGpsProviderEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGpsProviderEnabled) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_RATE_GPS_MS,
                    0.0f /* minDistance */,
                    mLocationListener);
            Log.d(TAG,"requestLocationUpdates");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //StartActivity._ws.send("Close:");
        StartActivity._ws.close(1000,"Close:");
    }
}