package com.example.android_andrey_client;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class SocketManager {

    public WebSocket Connect()
    {
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder()
                .url("ws://mrblackdog.ddnsking.com:5000/ws")
                .build();

        WebSocketGolos wsc = new WebSocketGolos();
        WebSocket ws = client.newWebSocket(request, wsc);
        return ws;
    }
}
