package com.example.android_andrey_client;

import android.util.Log;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

/**
 * Created by Игорь on 21.10.2018.
 */

public class WebSocketGolos extends WebSocketListener {

    private String TAG = "APP";
    /*private Color parseColor(String Message){
        int Red = Integer.parseInt(Message.split(",")[0]);
        int Green = Integer.parseInt(Message.split(",")[1]);
        int Blue = Integer.parseInt(Message.split(",")[2]);
        return new Color(Red/255f,Green/255f,Blue/255f,1);
    }*/
    public static MyCallBack getLocationCallBack;
    
    public interface  MyCallBack{
        void callBackCall(double latitude, double longitude);
    }

    WebSocketGolos()
    {
    }
    @Override
        public void onOpen(WebSocket webSocket, Response response) {
            Log.d(TAG,"Connection Opened");

    }
    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        super.onClosed(webSocket, code, reason);

    }

    @Override
    public void onMessage(WebSocket webSocket, String text)
    {
        String[] Mass = text.split(":");
        String Code = Mass[0];

        String[] Message = {};
        if (Mass.length > 1) {
            Message = Mass[1].split(" ");
            Log.d(TAG,Mass[1]);
        }
        getLocationCallBack.callBackCall(Double.parseDouble(Message[0]), Double.parseDouble(Message[1]));
    }
}
